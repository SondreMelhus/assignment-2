DROP TABLE IF EXISTS hero CASCADE;

CREATE TABLE hero
(
	hero_id SERIAL,
	hero_name VARCHAR (100) UNIQUE,
	hero_alias VARCHAR (50),
	hero_origin VARCHAR (999),
	CONSTRAINT pk_hero PRIMARY KEY (hero_id)
);

DROP TABLE IF EXISTS assistant CASCADE;

CREATE TABLE Assistant
(
	assistant_id SERIAL,
	assistant_name VARCHAR (100) UNIQUE,
	CONSTRAINT pk_assistant PRIMARY KEY (assistant_id)
	
);

DROP TABLE IF EXISTS power CASCADE;

CREATE TABLE Power
(
	power_id SERIAL,
	power_name VARCHAR (50) UNIQUE, 
	power_description VARCHAR (999),
	CONSTRAINT pk_power PRIMARY KEY (power_id)
);