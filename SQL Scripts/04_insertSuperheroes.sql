INSERT INTO hero (hero_alias, hero_name, hero_origin) VALUES ('Batman', 'Bruce Wayne', 'Childhood trauma');
INSERT INTO hero (hero_alias, hero_name, hero_origin) VALUES ('Superman', 'Clark Kent', 'Last survivor of Krypton');
INSERT INTO hero (hero_alias, hero_name, hero_origin) VALUES ('Captain America', 'Steve Rogers', 'Super soldier created during World War 2');
