DROP TABLE IF EXISTS hero_power;

CREATE TABLE hero_power
(
	hero_id INT,
	power_id INT,
	PRIMARY KEY (hero_id, power_id),
	CONSTRAINT fk_hero_id FOREIGN KEY (hero_id) REFERENCES hero(hero_id) ON UPDATE CASCADE ON DELETE CASCADE,
	CONSTRAINT fk_power_id FOREIGN KEY (power_id) REFERENCES power(power_id) ON UPDATE CASCADE ON DELETE CASCADE
);