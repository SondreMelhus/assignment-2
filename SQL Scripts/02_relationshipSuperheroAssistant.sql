ALTER TABLE assistant
ADD assistant_hero_id INT;

ALTER TABLE assistant 
ADD CONSTRAINT fk_assistant_hero_id
FOREIGN KEY (assistant_hero_id) 
REFERENCES hero(hero_id) 
ON DELETE CASCADE 
ON UPDATE CASCADE;


