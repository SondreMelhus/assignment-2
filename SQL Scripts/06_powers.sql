INSERT INTO power (power_name, power_description) VALUES ('Flight', 'Gives the hero the ability to fly');
INSERT INTO power (power_name, power_description) VALUES ('Super strength', 'Gives the hero super strength');
INSERT INTO power (power_name, power_description) VALUES ('Super intelligence', 'Gives the hero a super intellect');
INSERT INTO power (power_name, power_description) VALUES ('Money', 'Gives the hero way too much money');

INSERT INTO hero_power (hero_id, power_id) VALUES (1, 4);
INSERT INTO hero_power (hero_id, power_id) VALUES (1, 3);
INSERT INTO hero_power (hero_id, power_id) VALUES (2, 1);
INSERT INTO hero_power (hero_id, power_id) VALUES (2, 2);
INSERT INTO hero_power (hero_id, power_id) VALUES (3, 2);
