# Assignment 2

### Table of contents
* [Introduction](#introduction)
* [Appendix A](#appendix-a)
* [Appendix B](#appendix-b)
* [Methods](#methods)
* [Authors](#authors)


## Introduction

This project is created as a solution to Assignment 2 : Create a database and access it. Task given by Livinus Obiora Nweke, lecturer at Noroff University College, Full-Stack Java Development course.
The goal of the assignment is to create and access a local database, using Java, PgAdmin and  Spring Boot.

---

## Appendix A

In this task, we were tasked to create several scripts would create, alter and delete table, columns and relations in a database. The theme of the database is super heroes, their powers and assistants. A requirement for the task was to create a database in PgAdmin, and run all the scripts on this database.

**Definition of tables**

**Superhero** : Autoincremented integer Id, Name, Alias, Origin.
**Assitant** : Autoincremented integer Id, Name. 
**Power** :  Autoincremented integer Id, Name, Description.

**Script 1 : 01_tableCreate.sql:** Contains statements to create and set up the Superhero, assistant and power tables.

---

### Relations

Here we were tasked to include some relationships between the tables. The ALTER SQL keyword was used to change the 
existing tables to add the foreign keys needed to create a relation between a hero and their assistant. A description of the relationships can be seen below:

• One Superhero can have multiple assistants, one assistant has one superhero they assist.
• One Superhero can have many powers, and one power can be present on many Superheroes.

Firstly, to setup the Superhero-assistant relationship, we add a foreign key to assistant. This key points to a superheroes id, and creates a one-to-many relation. This allows a superhero to have multiple assitants, and an assistant can only assist one superhero.

**Script 2 : 02_relationshipSuperheroAssistent.sql:** Contains statmenets to ALTER any tables needed to add the foreign key and set up the constraint to configure the described relationship between Superhero and assistent.

Too implement the second clause, linking superheroes and their powers, we to add a linked table called Hero_Power. This table has a composite primary key, that consits of the primary key hero_id from hero, and power_id from power. This enables a many-to-many relation, allowing a hero to have mutiple powers, and a power to be assigned multiple heros.

**03_relationshipSuperheroPower.sql:** Contains statements to create the linking table between Superhero and Power. It contains ALTER statements to set up the forgein key constraints between the linking tables, Supero and Power tables.

---

### Inserting data

To populate the tables in the database, we wrote a three different scripts. These these scripts each inserts entities into each respective table: Hero, Assistant and Power.

**04_insertSuperhero.sql:** Script that inserts three new Superheroes into the database.

**05_insertAssistants.sql:** Inserts three assistants as well as which Superhero each of these can assist.

**06_powers.sql:** Inserts fours power and assigns them to Superheroes using the linking table.  

---

### Updating data

We were tasked with updating a superheroes name in out hero table. So we made a scripts that updates Bruce Wayne to Alfred Wayne.

**07_updateSuperhero.sql:** Contains statements to update a Superheros name

---

### Deleting data

Lastly we were tasked with deleting a assistant from the assistant table. So we made a scripts that deletes Robin which is Batmans assistant from the table.

**08_deleteAssistant.sql:** Contains statements to delete any assistent with a unique name. This could be improved by using an autoincrementor.

---

## Appendix B

###Overview

In this portion of the assignment we were tasked with manipulating SQL data in Spring using a the JDBC with the PostgreSQL 
driver. For this part of the assignment, we were given a database called Chinook. The Chinook database models the iTunes database of customers purchasing songs. We first needed to create the database in PgAdmin, then open a query tool and drag the Chinook script in and run it. This genereated all the tables and populated them.

###Requirements

For customers in the database, the following functionality should be catered for:

1. Read all the customers in the database, this should display their: Id, first name, last name, country, postal code, 
phone number and email.

2. Read a specific customer from the database (by Id), should display everything listed in the above point.

3. Read a specific customer by name. HINT: LIKE keyword can help for partial matches.

4. Return a page of customers from the database. This should take in limit and offset as parameters and make use 
of the SQL limit and offset keywords to get a subset of the customer data. The customer model from above 
should be reused.

5. Add a new customer to the database. You also need to add only the fields listed above (our customer object) 

6. Update an existing customer.

7. Return the country with the most customers.

8. Customer who is the highest spender (total in invoice table is the largest).

9. For a given customer, their most popular genre (in the case of a tie, display both). Most popular in this context 
means the genre that corresponds to the most tracks from invoices associated to that customer


###Repositories
We were expected to implement the repository pattern in this assignment. We implemented a generic CRUD parent, then 
one for customer that extends to the CRUD repository. All the functionality is contained in the single customer repository.


**Model for SQL database**

![image-3.png](./image-3.png)

### Methods
The application contains the following demanded methods:

**- findAll() :** Retrives all the customers stored in the database.

**- findById() :** Retirives a customer which has a id that matches the parameter id.

**- insert() :** Inserts a new customer into the database.

**- update() :** Updates customer in the database with new values.

**- getByName() :** Retrives a list of customers from the database table that has a first name or last name that matches the parameter.

**- getAPageOfCustomers() :** Retirives and prints a list of customers. The dimensions of the page are set by the limit and offset parameters.

**- findCountryWithMostCustomers() :** Retirves the country in the database that has the most amount of customers.

**- mostPopularGenre() :** Displays a customers most popular music genre. Defined by the amount of invoices they have for songs belonging to a given genre.

**- highestSpender() :** Retrived the customer that has spent the most money on tracks.

---

### DIRECTORY
```
README.md
image.png
Scripts/Part1
| 01_tableCreate.sql
| 02_relationshipSuperheroAssistent.sql
| 03_relationshipSuperheroPower.sql
| 04_insertSuperhero.sql
| 05_insertAssistants.sql
| 06_powers.sql
| 07_updateSuperhero.sql
| 08_deleteAssistant.sql
Assignment 2
| 
| .gitignore
| build.gradle
| gradlew
| gradlew.bat
| settings.gradle
|___ src
|      | 
|      |___ java/com/example/assignment_2
|      |     |___ Models
|      |     |    |__ Customer.java
|      |     |    |__ CustomerCountry.java
|      |     |    |__ CustomerRepositoryImplements.java
|      |     |    |__ CustomerSpender.java
|      |     |    |__ CustomerTrack.java
|      |     |
|      |     |___ Repositiries
|      |     |    |__ CRUDRepository.java
|      |     |    |__ CustomerRepository.java
|      |     |  
|      |     |___ Runner
|      |     |    |__ CustomerApplicationRunner.java
|      |     |
|      |     |___ Utility
|      |     |    |__ Printer
|      |     |
|      |     | Assignment2Application.java
|      |            
|      |___ resources
|      |            | application.properties
|___ java/com/example/assignment_2
|      |___ Assignment2Application.java
```

---

## Installation
**Installation of appendix A**
- Download the repository using SSH key
- Create a local or remote database
- Navigate to the SQL script folder
- Run every script in the folder using your databases Query tool

---

**Installation of appendix B**
- Download the repository using SSH key
- Create a local or remote database
- Open the project in IntelliJ Ultimate
- Open and run the SQL script in your database: chinook_pg_serial_pk_proper_naming.sql (We did not create this)
- Connect your Intellij Ultimate to your database
- Add the database connection credentials to application.properties file
- Run Main

---

## Unit testing
To run the unit tests:
- Complete installation of appendix B
- Navigate to the test directory in src
- Right-click the directory and press Run 'All Tests'
- Each test need to return a green check mark to pass the test

---

## Project status
Finished.

---

## License
This project is open-source. You are free to use any of the code in your own projects, as long as the work is credited.

---

## Authors and acknowledgment
**Code authors:**
- [Marius Eriksen](https://gitlab.com/marius.eriksen13)
- [Linnea Johansen](https://gitlab.com/LinneaJohansen)
- [Sondre Melhus](https://gitlab.com/SondreMelhus)

**Assignment given by:** Livinus Obiora Nweke, Lecturer at Noroff University College
