package com.example.assignment_2;

import com.example.assignment_2.Models.Customer;
import com.example.assignment_2.Models.CustomerCountry;
import com.example.assignment_2.Models.CustomerRepositoryImplements;
import com.example.assignment_2.Repositories.CustomerRepository;
import com.example.assignment_2.Runner.CustomerApplicationRunner;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.awt.*;
import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;

@SpringBootTest
class Assignment2ApplicationTests {

    @Autowired
    private CustomerRepository customerRepository;

    @Test
    void contextLoads() {
    }

    //Test for checking that the first element returns the expected customer
    @Test
    void checkingReadCustomerReadFirstCustomer_readsDatabase_returnsTrue() {
        //Arrange
        Customer expected = new Customer(1,"Luís","Gonçalves",
        "Brazil","12227-000","+55 (12) 3923-5555","luisg@embraer.com.br");

        //Act
        Customer actual = (Customer) customerRepository.findAll().get(0);

        //Assert
        assertEquals(expected,actual);
    }

    //Test for checking that first element does not give true for gibberish input
    @Test
    void checkingReadWrongCustomerReadFirstCustomer_readsDatabase_returnsFalse() {
        //Arrange
        Boolean expected = false;
        Customer customer1 = new Customer(44,"Terhi","Hämäläinen",
                "Finland","00530","+358 09 870 2000","@terhi.hamalainen@apple.fi");

        //Act
        Customer customer2 = (Customer) customerRepository.findAll().get(0);
        boolean actual = false;
        if(customer1.equals(customer2))actual=true;


        //Assert
        assertEquals(expected, actual);
    }

    //Test for checking that find by id returns correct customer
    @Test
    void checkValidIdReturn_sendCustomer_returnTrue(){
        //Arrange
        Customer expected = new Customer(1,"Luís","Gonçalves",
                "Brazil","12227-000","+55 (12) 3923-5555","luisg@embraer.com.br");

        //Act
        Customer actual = (Customer) customerRepository.findById(1);

        //Assert
        assertEquals(expected,actual);
    }

    //Test for checking that find by id returns correct customer
    @Test
    void checkInvalidIdReturn_sendCustomer_returnFalse(){
        //Arrange
        Customer customer1 = new Customer(1,"Luís","Gonçalves",
                "Brazil","12227-000","+55 (12) 3923-5555","luisg@embraer.com.br");
        boolean expected = false;

        //Act
        Customer customer2 = (Customer) customerRepository.findById(2);
        boolean actual = false;
        if (customer1.equals(customer2)) actual = true;

        //Assert
        assertEquals(expected,actual);
    }

    //Checking that it returns correct customer by name
    @Test
    void checkCustomerByName_sendName_returnTrue(){
        //Arrange
        Customer expected = new Customer(1,"Luís","Gonçalves",
                "Brazil","12227-000","+55 (12) 3923-5555","luisg@embraer.com.br");

        //Act
        Customer actual = customerRepository.getByName("Luís").get(0);

        //Assert
        assertEquals(expected,actual);
    }

    //Checking that unexpected customer is returned
    @Test
    void checkCustomerByName_sendName_returnFalse(){
        //Arrange
        Customer customer1 = new Customer(44,"Terhi","Hämäläinen",
                "Finland","00530","+358 09 870 2000","@terhi.hamalainen@apple.fi");
        boolean expected = false;

        //Act
        Customer customer2 = customerRepository.getByName("Luís").get(0);
        boolean actual = false;
        if (customer1.equals(customer2)) actual=true;

        //Assert
        assertEquals(expected,actual);
    }

    //Checks that method properly returns list
    @Test
    void checkCustomerPage_sendCustomers_returnTrue(){
        //Arrange
        List<Customer> expected = new ArrayList<>();
        Customer customer1 = new Customer(3, "François", "Tremblay", "Canada", "H2G 1A7",
                "+1 (514) 721-4711", "ftremblay@gmail.com");
        Customer customer2 = new Customer(4, "Bjørn", "Hansen",
                "Norway", "0171","+47 22 44 22 22", "bjorn.hansen@yahoo.no");
        Customer customer3 = new Customer(5,"František", "Wichterlová", "Czech Republic",
                "14700", "+420 2 4172 5555", "frantisekw@jetbrains.com");
        expected.add(customer1);
        expected.add(customer2);
        expected.add(customer3);

        //Act
        List<Customer> actual = customerRepository.getAPageOfCustomers(3,2);

        //Assert
        assertEquals(expected,actual);
    }

    //Checks that invalid objects aren't set as valid ones
    @Test
    void checkCustomerPage_sendWrongCustomers_returnFalse(){
        //Arrange
        List<Customer> list1 = new ArrayList<>();
        Customer customer1 = new Customer(44,"Terhi","Hämäläinen",
                "Finland","00530","+358 09 870 2000","@terhi.hamalainen@apple.fi");
        Customer customer2 = new Customer(4, "Bjørn", "Hansen",
                "Norway", "0171","+47 22 44 22 22", "bjorn.hansen@yahoo.no");
        Customer customer3 = new Customer(5,"František", "Wichterlová", "Czech Republic",
                "14700", "+420 2 4172 5555", "frantisekw@jetbrains.com");
        list1.add(customer1);
        list1.add(customer2);
        list1.add(customer3);
        boolean expected = false;

        //Act
        List<Customer> list2 = customerRepository.getAPageOfCustomers(3,2);
        boolean actual = false;
        if(list1.equals(list2)) actual=true;


        //Assert
        assertEquals(expected,actual);
    }

    //Test for checking that customer is added properly
    @Test
    public void addNewCustomer_checkIfCustomerIsAdded(){
        //Arrange
        Customer insertCustomer = new Customer(99999999, "Linnea", "Johanse", "Norway", "5050",
                "123456789", "linnea_johansen@outlook.com");
        int expected = 1;

        //Act
        int actual = customerRepository.insert(insertCustomer);

        //Assert
        assertEquals(expected,actual);
    }

    //Test for checking that customer is added properly
    @Test
    public void addNewCustomer_checkIfCustomerIsNotAddedOnFaultyId(){
        //Arrange
        Customer insertCustomer = new Customer(1, "Linnea", "Johanse", "Norway", "5050",
                "123456789", "linnea_johansen@outlook.com");
        int expected = 0;

        //Act
        int actual = customerRepository.insert(insertCustomer);

        //Assert
        assertEquals(expected,actual);
    }

    //Test for checking that customer is changed
    @Test
    public void updateCustomer_checkIfCustomerIsChanged(){
        //Arrange
        Customer updateCustomer = new Customer(4, "Bjørn", "Hansen",
                "Norway", "0171","+47 22 44 22 22", "bjorn.hansen@yahoo.no");
        int expected = 1;

        //Act
        int actual = customerRepository.update(updateCustomer);

        //Assert
        assertEquals(expected, actual);
    }

    //Test for checking country with most customers
    @Test
    public void customerCountryCheck(){
        //Arrange
        CustomerCountry expected = new CustomerCountry("USA");

        //Act
        CustomerCountry actual = customerRepository.findCountryWithMostCustomers();

        //Assert
        assertEquals(expected,actual);
    }

    //Test for checking country with most customers
    @Test
    public void customerCountryCheckInvalid(){
        //Arrange
        boolean expected = false;
        CustomerCountry testCountry = new CustomerCountry("Lichtensstein");

        //Act
        boolean actual = false;
        if(customerRepository.findCountryWithMostCustomers().equals(testCountry)) actual = true;

        //Assert
        assertEquals(expected,actual);
    }

    //Checking that highestSpender is valid
    @Test
    public void highestSpenderValid(){
        //Arrange
        Customer expected = new Customer(6, "Helena", "Holý", "Czech Republic",
                "14300", "+420 2 4177 0449", "hholy@gmail.com");

        //Act
        Customer actual = customerRepository.highestSpender();

        //Assert
        assertEquals(expected, actual);
    }

    //Checking that highestSpender is valid
    @Test
    public void highestSpenderInvalid(){
        //Arrange
        Customer customer = new Customer(4, "Bjørn", "Hansen",
                "Norway", "0171","+47 22 44 22 22", "bjorn.hansen@yahoo.no");
        boolean expected = false;

        //Act
        boolean actual = false;
        if(customerRepository.highestSpender().equals(customer)) actual = true;

        //Assert
        assertEquals(expected, actual);
    }

    //Test for verifying popular genre
    @Test
    public void customerGenreTestValid(){
        //Arrange
        String expected = "Rock";

        //Act
        String actual = customerRepository.mostPopularGenre(2);

        //Assert
        assertEquals(expected, actual);
    }

    //Test for verifying popular genre being invalid
    @Test
    public void customerGenreTestInvalid(){
        //Arrange
        String genre = "Metal";
        boolean expected = false;

        //Act
        boolean actual = false;
        if(customerRepository.mostPopularGenre(2).equals(genre)) actual = true;

        //Assert
        assertEquals(expected, actual);
    }
}
