package com.example.assignment_2.utility;

import com.example.assignment_2.Models.Customer;
import com.example.assignment_2.Models.CustomerCountry;

public class Printer <T> {

    //Method used to print a Customers attributes
    public static void printCustomer(Customer customer) {
        System.out.println("Customer id: " + customer.customer_id() + ", First name: " + customer.firstName() +
                ", Last name: " + customer.lastName() + ", Country: " + customer.country() + ", Postal code: " +
                customer.postalCode() + ", Phone number: " + customer.phoneNr() + ", Email: " + customer.email());
    }

}
