package com.example.assignment_2;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

//TODO: Implement all required methods in Customer interface
//TODO: Remove all unnecassary code in customerCountry, customerSpender, customerTrack (ONLY WHAT IS NEEDED)
//TODO: Set CustomerRepoImpl to @Primary repo


@SpringBootApplication
public class Assignment2Application {

    public static void main(String[] args) {
        SpringApplication.run(Assignment2Application.class, args);
    }

}
