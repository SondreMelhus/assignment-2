package com.example.assignment_2.Models;

public record Customer(int customer_id,
                       String firstName,
                       String lastName,
                       String country,
                       String postalCode,
                       String phoneNr,
                       String email) {
}
