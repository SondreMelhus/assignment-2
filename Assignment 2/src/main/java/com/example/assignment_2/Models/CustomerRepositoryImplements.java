package com.example.assignment_2.Models;

import com.example.assignment_2.Repositories.CustomerRepository;
import com.example.assignment_2.utility.Printer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Repository;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

import static java.lang.Integer.valueOf;

@Repository
public class CustomerRepositoryImplements implements CustomerRepository {
    private String url;
    private String username;
    private String password;

    @Autowired
    public void CustomerRepositoryImpl(
            @Value("${spring.datasource.url}") String url,
            @Value("${spring.datasource.username}") String username,
            @Value("${spring.datasource.password}") String password) {
        this.url = url;
        this.username = username;
        this.password = password;
    }

    @Override
    //Get all customers
    public List<Customer> findAll() {
        //Order by id
        String sql = "SELECT * FROM customer ORDER BY customer_id";

        //List to store customers
        List<Customer> customers = new ArrayList<>();

        //Check connection
        try (Connection conn = DriverManager.getConnection(url, username, password);) {

            //Write statement
            PreparedStatement statement = conn.prepareStatement(sql);

            //Execute statement
            ResultSet result = statement.executeQuery();

            //Handle result
            while (result.next()) {
                //Create customer object for each found customer
                Customer customer = new Customer(
                        result.getInt("customer_id"),
                        result.getString("first_name"),
                        result.getString("last_name"),
                        result.getString("country"),
                        result.getString("postal_code"),
                        result.getString("phone"),
                        result.getString("email")
                );
                //Add customer to list
                customers.add(customer);

                //Print list
                Printer.printCustomer(customer);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return customers;
    }

    @Override
    //Find customer by id
    public Customer findById(Integer id) {

        //Create a customer object
        Customer customer = null;

        //Get customer based on customer_id
        String sql = "SELECT * FROM customer WHERE customer_id = ?";

        //Check connection
        try (Connection conn = DriverManager.getConnection(url, username, password);) {

            //Write statement
            PreparedStatement statement = conn.prepareStatement(sql);
            statement.setInt(1,id);

            //Execute statement
            ResultSet result = statement.executeQuery();

            //Handle result
            result.next();

            //Customer object for the found customer
            customer = new Customer(
                    result.getInt("customer_id"),
                    result.getString("first_name"),
                    result.getString("last_name"),
                    result.getString("country"),
                    result.getString("postal_code"),
                    result.getString("phone"),
                    result.getString("email")
                );
            Printer.printCustomer(customer);
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return customer;
    }

    @Override
    //Method used to insert a new customer into the table
    public int insert(Customer customer) {

        //SQL query that inserts the given values into the table
        String sql = "INSERT INTO customer (customer_id, first_name, last_name, " +
                     "country, postal_code, phone, email) VALUES (?,?,?,?,?,?,?)";

        //Integer used to track if insertion was successful
        int result = 0;

        //Check connection
        try (Connection conn = DriverManager.getConnection(url, username, password);) {

            //Write statement
            PreparedStatement statement = conn.prepareStatement(sql);

            //Inject customer attributes into the statement at given positions
            statement.setInt(1, customer.customer_id());
            statement.setString(2, customer.firstName());
            statement.setString(3, customer.lastName());
            statement.setString(4, customer.country());
            statement.setString(5, customer.postalCode());
            statement.setString(6, customer.phoneNr());
            statement.setString(7, customer.email());

            //Execute statement
            result = statement.executeUpdate();

        } catch (SQLException e) {
            e.printStackTrace();
        }

        //Gives feedback if insertion was successful
        if (result == 1) {
            System.out.println("Insertion was successful");
        } else {
            System.out.println("Insertion was not was successful");
        }

        return result;
    }


    @Override
    //Updates an entity in the database table
    public int update(Customer customer) {
        //Update customer
        String sql = "UPDATE customer SET customer_id = (?), first_name = (?),last_name = (?), country = (?), postal_code = (?), phone = (?), email = (?) WHERE customer.customer_id = (?) ";
        int result = 0;

        //Check connection
        try (Connection conn = DriverManager.getConnection(url, username, password);) {

            //Write statement
            PreparedStatement statement = conn.prepareStatement(sql);
            statement.setInt(1, customer.customer_id());
            statement.setString(2, customer.firstName());
            statement.setString(3, customer.lastName());
            statement.setString(4, customer.country());
            statement.setString(5, customer.postalCode());
            statement.setString(6, customer.phoneNr());
            statement.setString(7, customer.email());
            statement.setInt(8, customer.customer_id());

            //Execute
            result = statement.executeUpdate();

        } catch (SQLException e) {
            e.printStackTrace();
        }

        //Gives feedback if insertion was successful
        if (result == 1) {
            System.out.println("Update was successful");
        } else {
            System.out.println("Update was not was successful");
        }
        return result;
    }

    @Override
    public int delete(Customer customer) {
        return 0;
    }

    @Override
    public int deleteById(Integer id) {
        return 0;
    }

    @Override
    //Return all customers that are like input
    public List<Customer> getByName(String name) {

        List<Customer> customers = new ArrayList<Customer>();
        String sql = "SELECT * FROM customer WHERE first_name LIKE (?) OR last_name LIKE (?)";

        //Check connection
        try (Connection conn = DriverManager.getConnection(url, username, password);) {

            //Write statement
            PreparedStatement statement = conn.prepareStatement(sql);
            statement.setString(1,name);
            statement.setString(2, name);
            System.out.println(statement);

            //Execute statement
            ResultSet result = statement.executeQuery();

            //Handle result
            while (result.next()) {
                //New customer object for each customer
                Customer customer = new Customer(
                        result.getInt("customer_id"),
                        result.getString("first_name"),
                        result.getString("last_name"),
                        result.getString("country"),
                        result.getString("postal_code"),
                        result.getString("phone"),
                        result.getString("email")
                );
                //Add cusomer to list
                customers.add(customer);
                Printer.printCustomer(customer);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return customers;
    }

    @Override
    //Returns a list of customers that match the dimensions of a page.
    //The dimensions of the page are set by limit and offset.
    public List<Customer> getAPageOfCustomers(int limit, int offset) {

        //List to store  customers
        List<Customer> customerPage = new ArrayList<>();

        //Get customers from offset to limit
        String sql = "SELECT * \n" +
                "FROM customer\n" +
                "limit (?) OFFSET (?)";

        //Check connection
        try (Connection conn = DriverManager.getConnection(url, username, password);) {

            //Write statement
            PreparedStatement statement = conn.prepareStatement(sql);
            statement.setInt(1, limit);
            statement.setInt(2, offset);
            System.out.println(statement);

            //Execute statement
            ResultSet result = statement.executeQuery();

            //Handle result
            while (result.next()) {
                Customer customer = new Customer(
                        result.getInt("customer_id"),
                        result.getString("first_name"),
                        result.getString("last_name"),
                        result.getString("country"),
                        result.getString("postal_code"),
                        result.getString("phone"),
                        result.getString("email")
                );
                customerPage.add(customer);
                Printer.printCustomer(customer);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return customerPage;
    }

    @Override
    //Returns the country with the most customer.
    public CustomerCountry findCountryWithMostCustomers() {

        //Get all Customers and order by country descending
        String sql = "SELECT * FROM customer order by country DESC ";

        //Store countries
        CustomerCountry country = null;

        //Check connection
        try (Connection conn = DriverManager.getConnection(url, username, password);) {

            //Write statement
            PreparedStatement statement = conn.prepareStatement(sql);

            //Execute statement
            ResultSet result = statement.executeQuery();

            //Handle result
            result.next();
            Customer customer = new Customer(
                    result.getInt("customer_id"),
                    result.getString("first_name"),
                    result.getString("last_name"),
                    result.getString("country"),
                    result.getString("postal_code"),
                    result.getString("phone"),
                    result.getString("email")
            );

            //Use the customers country to create a new CustomerCountry
            country = new CustomerCountry(customer.country());


        } catch (SQLException e) {
            e.printStackTrace();
        }

        //Print the country with the most customers
        System.out.println("The country with the most customer is " + country.country());
        return country;
    }

    @Override
    //Take in id if the person you want to get most popular songs for
    public String mostPopularGenre(Integer id) {
        //variable for saving genre later
        String genre = "";

        //Get most popular genre for given person
        String sql = "SELECT COUNT(track.genre_id) AS pop_genre, genre.\"name\" AS name\n" +
                "FROM customer\n" +
                "INNER JOIN invoice\n" +
                "ON invoice.customer_id = customer.customer_id\n" +
                "INNER JOIN invoice_line \n" +
                "ON invoice_line.invoice_id = invoice.invoice_id\n" +
                "INNER JOIN track \n" +
                "ON track.track_id = invoice_line.track_id\n" +
                "INNER JOIN genre ON genre.genre_id = track.genre_id\n" +
                "WHERE customer.customer_id = (?)\n" +
                "GROUP BY genre.genre_id\n" +
                "ORDER BY pop_genre desc\n" +
                "FETCH first 1 rows with ties ";

        //Check connection
        try (Connection conn = DriverManager.getConnection(url, username, password);) {

            //Write statement
            PreparedStatement statement = conn.prepareStatement(sql);
            statement.setInt(1,id);

            //Execute statement
            ResultSet result = statement.executeQuery();
            result.next();

            //Handle result
            genre = result.getString("name");
            System.out.println(genre);

        } catch (SQLException e) {
            e.printStackTrace();
        }
        //Display result
        System.out.println("Your most popular genre is: "+genre);
        return genre;
    }

    @Override
    //Method for finding the highest spender
    public Customer highestSpender(){

        //Get the customer that has spent the most money
        String query = "SELECT cu.* FROM invoice AS i\n" +
                "INNER JOIN customer as cu ON i.customer_id = cu.customer_id\n" +
                "GROUP BY cu.customer_id ORDER BY SUM(total) DESC FETCH first 1 rows with ties";

        //Check connection
        try(Connection conn = DriverManager.getConnection(url, username, password)) {

            //Write statement
            PreparedStatement statement= conn.prepareStatement(query);

            //Execute
            ResultSet result= statement.executeQuery();
            result.next();

            //Creates a Customer to return based on output
            Customer spender = new Customer(
                    result.getInt("customer_id"),
                    result.getString("first_name"),
                    result.getString("last_name"),
                    result.getString("country"),
                    result.getString("postal_code"),
                    result.getString("phone"),
                    result.getString("email")
            );
            return spender;
        }catch (SQLException e){
            e.printStackTrace();
        }
        return null;
    }

    @Override
    //Tests if Spring boot can establish a connection to the database.
    public void test() {
        //Try connecting with url, username and password
        try (Connection conn = DriverManager.getConnection(url, username, password);) {
            System.out.println("Connected to Postgres...");

            //exception if it can|t connect
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

}
