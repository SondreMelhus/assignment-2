package com.example.assignment_2.Models;

import java.math.BigDecimal;
import java.sql.Timestamp;


public record CustomerSpender(int invoiceId,
                              int customerId,
                              Timestamp date,
                              String billingAddress,
                              String billingCity,
                              String billingState,
                              String billingCountry,
                              String billingPostalCode,
                              BigDecimal total) {
}
