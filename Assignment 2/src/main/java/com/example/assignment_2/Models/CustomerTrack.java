package com.example.assignment_2.Models;

import java.math.BigDecimal;

public record CustomerTrack(int trackId,
                            String name,
                            int albumId,
                            int mediaType,
                            int genreId,
                            String composer,
                            int milliseconds,
                            int bytes,
                            BigDecimal price) {
}
