package com.example.assignment_2.Repositories;

import org.springframework.stereotype.Service;

import java.util.List;

@Service
public interface CRUDRepository <T, U>{


    /**
     * Returns a list all entities that are stored in a database table.
     * <p>
     *
     * </p>
     * This method throws SQLException if Spring boot cannot establish a connection to the database.
     * This can be caused by lacking or incorrect connection information (URL, Username or password).
     * This information can be set in resources/application.properties file.
     *
     * @param
     * @return          List of all entities in database table
     */
    List<T> findAll();


    /**
     * Returns a specific entity from the database table that has a primary key (id) that matches the
     * parameter id. If the database table does not contain a entity with the given id, the method will
     * return null.
     * <p>
     *
     * </p>
     * This method throws SQLException if Spring boot cannot establish a connection to the database.
     * This can be caused by lacking or incorrect connection information (URL, Username or password).
     * This information can be set in resources/application.properties file.
     * @param id    Integer that represents the id of the entity we want to find
     * @return      Entity in the database table that has a matching id, or null if no enitiy with that id exists
     */
    T findById(U id);


    /**
     * Inserts the parameter object in the database table as a new entity.
     * <p>
     *
     * </p>
     * This method throws SQLException if Spring boot cannot establish a connection to the database.
     * This can be caused by lacking or incorrect connection information (URL, Username or password).
     * This information can be set in resources/application.properties file.
     * @param object    Object that describes the entity, its attributes defined by a pre-made record
     * @return integer  Returns 1 if insertion was successful, 0 if unsuccessful
     */
    int insert(T object);


    /**
     * Updates a entity in the database table to match the attributes of the parameter object.
     * <p>
     *
     * </p>
     * This method throws SQLException if Spring boot cannot establish a connection to the database.
     * This can be caused by lacking or incorrect connection information (URL, Username or password).
     * This information can be set in resources/application.properties file.
     * @param object    Object that describes the entity, its attributes defined by a pre-made record
     * @return Integer  Returns 1 if update was successful, 0 if unsuccessful
     */
    int update(T object);


    /**
     * Deletes a entity in the database table that matches the attributes of the parameter object.
     * <p>
     *
     * </p>
     * This method throws SQLException if Spring boot cannot establish a connection to the database.
     * This can be caused by lacking or incorrect connection information (URL, Username or password).
     * This information can be set in resources/application.properties file.
     * @param object    Object that describes the entity, its attributes defined by a pre-made record
     * @return Integer  Returns 1 if deletion was successful, 0 if unsuccessful
     */
    int delete(T object);

    /**
     * Deletes a entity in the database table that has an id that matches the parameter id
     * <p>
     *
     * </p>
     * This method throws SQLException if Spring boot cannot establish a connection to the database.
     * This can be caused by lacking or incorrect connection information (URL, Username or password).
     * This information can be set in resources/application.properties file.
     * @param id        Integer that represents the id of the entity we want to delete
     * @return Integer  Returns 1 if deletion was successful, 0 if unsuccessful
     */
    int deleteById(U id);
}
