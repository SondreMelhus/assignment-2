package com.example.assignment_2.Repositories;

import com.example.assignment_2.Models.Customer;
import com.example.assignment_2.Models.CustomerCountry;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public interface CustomerRepository extends CRUDRepository<Customer, Integer>{

     /**
      * Tests if Spring boot can establish a connection to the database.
      * <p>
      *
      * </p>
      * This method throws SQLException if Spring boot cannot establish a connection to the database.
      * This can be caused by lacking or incorrect connection information (URL, Username or password).
      * This information can be set in resources/application.properties file.
      */
     void test();

     /**
      * Returns a list of customers from the database table that has a first name or last name that matches the parameter.
      * <p>
      *
      * </p>
      * This method throws SQLException if Spring boot cannot establish a connection to the database.
      * This can be caused by lacking or incorrect connection information (URL, Username or password).
      * This information can be set in resources/application.properties file.
      * @param name    String that contains the first name or last name of the customer you want to find in the database table.
      * @return List<Customer>     Returns a list of all customers that has a first name or last name that matches the parameter.
      */
     List<Customer> getByName(String name);

     /**
      * Returns a list of customers that match the dimensions of a page. The dimensions of the page are set by
      * limit and offset.
      * <p>
      *
      * </p>
      * This method throws SQLException if Spring boot cannot establish a connection to the database.
      * This can be caused by lacking or incorrect connection information (URL, Username or password).
      * This information can be set in resources/application.properties file.
      * @param limit     Integer that defines of many customers that should make up a page.
      * @param offset    Integer that defines how many customers we should skip before we collect customer for the page
      * @return List<Customer>     A list of all the customers that are part of the page
      */
     List<Customer> getAPageOfCustomers(int limit, int offset);

     /**
      * Returns the country with the most customer.
      * <p>
      *
      * </p>
      * This method throws SQLException if Spring boot cannot establish a connection to the database.
      * This can be caused by lacking or incorrect connection information (URL, Username or password).
      * This information can be set in resources/application.properties file.
      * @return customerCountry    The country that has the most customers.
      */
     CustomerCountry findCountryWithMostCustomers();

     /**
      * Returns the customer that has spent the most money on tracks.
      * <p>
      *
      * </p>
      * This method throws SQLException if Spring boot cannot establish a connection to the database.
      * This can be caused by lacking or incorrect connection information (URL, Username or password).
      * This information can be set in resources/application.properties file.
      * @return customer The customer that has spent the most amount of money on tracks.
      */
     Customer highestSpender();

     /**
      * Displays a customers most popular music genre. Defined by the amount of invoices they have for songs
      * belonging to a given genre.
      * <p>
      *
      * </p>
      * This method throws SQLException if Spring boot cannot establish a connection to the database.
      * This can be caused by lacking or incorrect connection information (URL, Username or password).
      * This information can be set in resources/application.properties file.
      */
     String mostPopularGenre (Integer id);

}
