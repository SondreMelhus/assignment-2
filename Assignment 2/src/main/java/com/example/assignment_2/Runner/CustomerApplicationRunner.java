package com.example.assignment_2.Runner;

import com.example.assignment_2.Models.Customer;
import com.example.assignment_2.Repositories.CustomerRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.stereotype.Component;

@Component
public class CustomerApplicationRunner implements ApplicationRunner {

    private  CustomerRepository customerRepository;

    @Autowired
    public CustomerApplicationRunner(CustomerRepository customerRepository) {
        this.customerRepository = customerRepository;
    }

    @Override
    public void run(ApplicationArguments args) throws Exception {

        customerRepository.findAll();
    
    }
}
